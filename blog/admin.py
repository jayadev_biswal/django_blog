from django.contrib import admin

# Register your models here.

from blog.models import ArticleModel
@admin.register(ArticleModel)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ("titles","auther","created_at")