from django.db import models

# Create your models here.

class ArticleModel(models.Model):
    titles = models.CharField(max_length=100)
    category = models.CharField(max_length=100)
    auther = models.CharField(max_length=100)
    contents = models.TextField()
    created_at = models.DateTimeField()

    def __str__(self):
        return self.titles