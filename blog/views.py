



# Create your views here.
from django.http import HttpResponse, HttpResponseNotFound
from django.utils import timezone
from django.views import View
from datetime import datetime
from blog.models import ArticleModel
from django.shortcuts import render, redirect
from blog.forms import ArticleForm

class Home(View):
    def get(self,request):
        return HttpResponse("Welcome To My Blog")
    def post(self, request):
        return HttpResponse("[Post]Welcome To My Blog")

# def home(request):
#     if request.method == 'GET':
#         return HttpResponse("Welcome To My Blog")
#     if request.method == 'POST':
#         return HttpResponse("[POST] Welcome to my blog")

class Article(View):
    def get(self, request):
        articles = ArticleModel.objects.all()
        print(articles)
        return render(request, "articles.html", {"articles":articles, "form": ArticleForm()})

    def post(self, request):
        form = ArticleForm(request.POST)
        form.instance.created_at = datetime.now(tz=timezone.utc)
        form.save()
        return redirect("/blog/articles")

class ArticleDetails(View):
    def get(self, request, id):
        try:
            articles = ArticleModel.objects.get(id=id)
            return render(request, "article_details.html", {"articles": articles})
        except ArticleModel.DoesNotExist:
            return HttpResponseNotFound()
