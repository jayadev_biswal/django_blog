from datetime import datetime
from django.utils import timezone
from django.test import TestCase
from blog.models import ArticleModel
class ArticleTest(TestCase):
    def test_article_created_success(self):
        ArticleModel.objects.create(titles="test title",category="test category",auther="test auther",contents="test content",created_at=datetime.now(tz=timezone.utc))
        articles = ArticleModel.objects.get(titles="test title")
        self.assertEqual(articles.category, "test category")

class BlogPagesTest(TestCase):
    def test_home_page_content(self):
        res = self.client.get("/blog/")
        self.assertEqual(res.content, b"Welcome To My Blog")
